<?
use yii\helpers\Url;
use app\components\modalComponent;
?>

<div class="">

    <h1 class="text-center text-dark">Editar Administradoras</h1>

    <div class="">
        <form id="form-adm" class="col-12 py-2 mt-4 mb-4 rounded" action="<?= Url::to(['administradoras/realiza-edicao-administradoras']); ?>" method="post">

            <input class="col col-12 mt-2 form-control" type="text" name="nomeAdm" value="<?= $edit['nomeAdm'] ?>" placeholder="Nome da Adm" required>

            <input class="col col-12 mt-2 form-control" type="text" name="cnpj" value="<?= $edit['cnpj'] ?>" placeholder="CNPJ" required>

            <input type="hidden" name="<?= yii::$app->request->csrfParam; ?>" value="<?= yii::$app->request->csrfToken; ?>">

            <input type="hidden" name="id" value="<?=$edit['id']?>">

            <div class="col col-12 mt-2 mx-0 form-group">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button class="btn btn-primary buttonEnviar" type="submit">Salvar</button>
            </div>
            <!-- <button class="btn btn-info mt-2 col col-12 buttonEnviar" type="submit">Enviar</button> -->
        </form>
    </div>

</div>