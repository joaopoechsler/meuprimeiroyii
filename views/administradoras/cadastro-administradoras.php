<?
use yii\helpers\Url;
?>

<div class="">

    <h1 class="text-center text-dark">Cadastro de Administradoras</h1>

        <div class="">
            <form id="form-adm" class="col-12 py-2 mt-4 mb-4 rounded shadow" action="<?=Url::to(['administradoras/realiza-cadastro-administradora']);?>" method="post">

                <input class="col col-12 mt-2 form-control" type="text" name="nomeAdm" value="" placeholder="Nome da Adm" required>
                
                <input class="col col-12 mt-2 form-control" type="text" name="cnpj" value="" placeholder="CNPJ"required>       

                <input type="hidden" name="<?=yii::$app->request->csrfParam;?>" value="<?=yii::$app->request->csrfToken;?>">

                <button class="btn btn-info mt-2 col col-12 buttonEnviar" type="submit">Enviar</button>
            </form>
        </div>
        
</div>