<?
use app\components\maskComponent;
use app\components\modalComponent;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\url;
use app\controllers\maskController;

$url_site = Url::base($schema = true);

?>
<h1 class="text-center">Administradoras</h1>
<ul>

<table class="col col-12 table table-striped table-dark table-responsive-md mt-3" id="listaClientes">
    <tr>
        <td>Nome</td>
        <td>CNPJ</td>
        <td>Data Cadastro</td>
        <td align="center"><a href="#" class="btn btn-info btn-sm">Adicionar</a></td>
    </tr>
    <? foreach ($administradoras as $dados) {?>
        <tr data-id="<?=$dados['id']?>">
            <td><?= $dados['nomeAdm'] ?></td>
            <td><?= maskComponent::mask($dados['cnpj'], 'cnpj') ?></td>
            <td><?=Yii::$app->formatter->format($dados['dataCadastro'],'date')?></td>
            <td align="center">
                <a href="<?=$url_site?>/index.php?r=administradoras/edita-administradoras&id=<?=$dados['id']?>" class="openModal"><i class="bi bi-pencil-square text-info"></i></a>
                <a href="#" data-id="<?=$dados['id']?>" class="removerCliente"><i class="bi bi-trash-fill text-info"></i></a>
            </td>
        </tr>
    <? } ?>

</table>

<div class="totalRegistros col-12 float-right">
    Total Registros <?=$paginacao->totalCount?>
</div>

<div class="row">
    <div class="col-12 mt-2">
            <?= LinkPager::widget(
            [
                'pagination' => $paginacao, 
                'linkContainerOptions' => [
                    'class' => 'page-item bg-dark text-info border-info'
                    ]
                , 'linkOptions' => [
                    'class' => 'page-link bg-dark text-info border-info'
                ],
                'disabledListItemSubTagOptions' => [
                    'class' => 'page-link bg-dark text-info border-info'
                ]
            ]
            ) ?>
    </div>
</div>

<?=modalComponent::initModal()?>
