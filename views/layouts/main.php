<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="d-flex flex-column h-100 mb-2">
    <?php $this->beginBody() ?>

    <header>
        <?php
        NavBar::begin([
            'brandLabel' => '<img src="img/Logo-site.png" width="70px"/>',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar navbar-expand-md znavbar-dark bg-dark fixed-top',
            ],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => [
                [
                    'label' => 'Condominios',
                    'items' => [
                        [
                            'label' => 'Listar',
                            'url' => ['condominios/listar-condominios']
                        ], [
                            'label' => 'Cadastrar Condominios',
                            'url' => ['condominios/cadastro-condominios']
                        ]
                    ]
                ],
                [
                    'label' => 'Blocos',
                    'items' => [
                        [
                            'label' => 'Listar',
                            'url' => ['blocos/listar-blocos']
                        ], [
                            'label' => 'Cadastrar Blocos',
                            'url' => ['blocos/cadastro-blocos']
                        ]
                    ]
                ],
                [
                    'label' => 'Unidade',
                    'items' => [
                        [
                            'label' => 'Listar',
                            'url' => ['unidades/listar-unidades']
                        ], [
                            'label' => 'Cadastrar Unidade',
                            'url' => ['unidades/cadastro-unidades']
                        ]
                    ]
                ],
                [
                    'label' => 'Morador',
                    'items' => [
                        [
                            'label' => 'Listar',
                            'url' => ['moradores/listar-moradores']
                        ], [
                            'label' => 'Cadastrar Morador',
                            'url' => ['moradores/cadastro-moradores']
                        ]
                    ]
                ],
                [
                    'label' => 'Conselho',
                    'items' => [
                        [
                            'label' => 'Listar',
                            'url' => ['/site/list-conselho']
                        ], [
                            'label' => 'Cadastrar Conselho',
                            'url' => ['site/cad-conselho']
                        ]
                    ]
                ],
                Yii::$app->user->isGuest ? (['label' => 'Login', 'url' => ['/site/login']]
                ) : ('<li>'
                    . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                    . Html::submitButton(
                        'Logout (' . Yii::$app->user->identity->usuario . ')',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>'
                )
            ],
        ]);
        NavBar::end();
        ?>
    </header>

    <main role="main" class="flex-shrink-0 mt-2 mb-2">
        <div class="container justify-content-center">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </main>

    <footer class="footer fixed-bottom py-2 text-muted bg-dark text-light" style="height: 40px;">
        <div class="container">
            <p class="float-left">&copy; ApCoders <?= date('Y') ?></p>
            <p class="float-right">Jao</p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>