<?

use app\components\generoComponent;
use app\components\modalComponent;
use app\components\selectedComponent;
use app\controllers\BlocosController;
use app\controllers\CondominiosController;
use app\controllers\UnidadesController;
use yii\helpers\Url;
?>

<h1 class="text-center text-dark">Editar Morador</h1>
<form id="form-clientes" class="col-12 px-2 py-2 mt-0" action="<?= Url::to(['moradores/realiza-edicao-moradores']); ?>" method="post">

    <select name="from_condominio" class="custom-select mt-2 fromCondominio">
    <option value="">Selecione um Condomínio</option>
                <? 
               foreach(CondominiosController::listaCondominiosSelect() as $cond){?>
                <option value="<?=$cond['id']?>"<?=selectedComponent::isSelected($cond['id'], $edit['from_condominio'])?>><?=$cond['nome']?></option>
                <? } ?>
    </select>

    <select name="from_bloco" class="fromBloco custom-select mt-2">
        <?
        foreach (BlocosController::listaBlocosEdit($edit['from_bloco']) as $bloco) {
            echo '<option value="' . $bloco['id'] . '"' .selectedComponent::isSelected($bloco['id'], $edit['from_bloco']) . '>' . $bloco['nomeBloco'] . '</option>';
        }
        ?>
    </select>

    <select name="from_unidade" class="fromUnidade custom-select mt-2">
    <?
                foreach(UnidadesController::listaUnidadesSelect($edit['from_condominio']) as $und){
                    echo '<option value="'.$und['id'].'"'.selectedComponent::isSelected($und['id'], $edit['from_unidade']).'>'.$und['numUnd'].'</option>';
                }
                ?>
    </select>

    <input class="col col-12 mt-2 form-control" type="text" name="nomeMorador" value="<?= $edit['nomeMorador'] ?>" placeholder="Nome" required>

    <input class="col col-12 mt-2 form-control px-0 py-2" type="date" name="nascimento"  value="<?= $edit['nascimento'] ?>">

    <div class="col col-12 mt-2 form-group px-0">
        <?= generoComponent::genSelect('genero', $edit['genero']) ?>
    </div>

    <input class="col col-12 mt-2 form-control" type="text" name="cpf" value="<?= $edit['cpf'] ?>" placeholder="CPF" required>
    <input class="col col-12 mt-2 form-control" type="email" name="email" value="<?= $edit['email'] ?>" placeholder="Email" required>
    <input class="col col-12 mt-2 form-control" type="text" name="telefone" value="<?= $edit['telefone'] ?>" placeholder="Telefone">

    <input type="hidden" name="id" value="<?=$edit['id']?>">

    <input type="hidden" name="<?= yii::$app->request->csrfParam; ?>" value="<?= yii::$app->request->csrfToken; ?>">

    <div class="col col-12 mt-2 mx-0 form-group">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button class="btn btn-primary buttonEnviar" type="submit">Salvar</button>
    </div>

    <!-- <button class="btn btn-info mt-2 col col-12 buttonEnviar" type="submit">Enviar</button> -->
</form>

</div>