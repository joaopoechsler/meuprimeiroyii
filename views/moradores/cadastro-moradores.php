<?
use app\components\generoComponent;
use app\components\modalComponent;
use app\controllers\BlocosController;
use app\controllers\CondominiosController;
use app\controllers\UnidadesController;
use yii\helpers\Url;

?>

<h1 class="text-center text-dark">Cadastro de Moradores</h1>
<form id="form-clientes" class="col-12 px-2 py-2 mt-4 mb-4 rounded shadow" action="<?=Url::to(['moradores/realiza-cadastro-moradores']);?>" method="post">

    <select name="from_condominio" class="custom-select mt-2 fromCondominio">
        <option value="">Selecione o Condominio</option>
        <?
        foreach (CondominiosController::listaCondominiosSelect() as $condo) {
        ?>
            <option value="<?= $condo['id'] ?>"><?= $condo['nome'] ?></option>
        <? } ?>
    </select>

    <select name="from_bloco" class="fromBloco custom-select mt-2">

    </select>

    <select name="from_unidade" class="fromUnidade custom-select mt-2">

    </select>

    <input class="col col-12 mt-2 form-control" type="text" name="nomeMorador" value="" placeholder="Nome" required>

    <input class="col col-12 mt-2 form-control px-0 p-2" type="date" name="nascimento"  value="">
    
    <div class="col col-12 mt-2 form-group px-0">
            <?=generoComponent::genSelect('genero')?>
    </div>

    <input class="col col-12 mt-2 form-control" type="text" name="cpf" value="" placeholder="CPF" required>
    <input class="col col-12 mt-2 form-control" type="email" name="email" value="" placeholder="Email" required>
    <input class="col col-12 mt-2 form-control" type="text" name="telefone" value="" placeholder="Telefone">

    <input type="hidden" name="<?= yii::$app->request->csrfParam; ?>" value="<?= yii::$app->request->csrfToken; ?>">

    <button class="btn btn-info mt-2 col col-12 buttonEnviar" type="submit">Enviar</button>
    
</form>