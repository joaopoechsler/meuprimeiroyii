<?
use app\components\alertComponent;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\url;
use app\components\maskComponent;
use app\components\modalComponent;

$url_site = Url::base(true);

// echo alertComponent::myAlert('success', 'Testando', '?r=moradores%2Fcadastro-moradores');
?>
<h1 class="text-center">Moradores</h1>

<table class="col col-md-12 col-sm-10 table table-dark table-striped table-responsive-md table-responsive-lg mt-4 shadow" id="listaClientes">

    <tr>
        <td>Condominio</td>
        <td>Bloco</td>
        <td>Unidade</td>
        <td>Nome</td>
        <td>Documento</td>
        <td>Email</td>
        <td>Telefone</td>
        <td>Genero</td>
        <td>Data Cadastro</td>
        <td>Data Atualizado</td>
        <td align="center"><a href="<?=$url_site?>cadastroMoradores" class="btn btn-info">Cadastrar</a></td>
    </tr>

    <?
    foreach ($moradores as $dados) {
    ?>
        <tr data-id="<?=$dados['id']?>">
            <td><?= $dados['nome'] ?></td>
            <td><?= $dados['nomeBloco'] ?></td>
            <td><?= $dados['numUnd'] ?></td>
            <td><?= $dados['nomeMorador'] ?></td>
            <td><?= maskComponent::mask($dados['cpf'], 'cpf') ?></td>
            <td><?= $dados['email'] ?></td>
            <td><?= maskComponent::mask($dados['telefone'], 'telefone') ?></td>
            <td><?= $dados['genero'] ?></td>
            <td><?=Yii::$app->formatter->format($dados['dataCadastro'],'date')?></td>
                <td align="center">
                    <a href="<?=$url_site?>/index.php?r=moradores/edita-moradores&id=<?=$dados['id']?>" class="openModal"><i class="bi bi-pencil-square text-info"></i></a>
                    <a href="#" data-id="<?= $dados['id'] ?>" class="removerCliente"><i class="bi bi-trash-fill text-info"></i></a>
                </td>
            <td colspan="8"></td>
        </tr>
    <? } ?>
</table>  

<div class="totalRegistros col-12 float-right">
    Total Registros <?=$paginacao->totalCount?>
</div>

<div class="row">
    <div class="col-12 mt-2">
            <?= LinkPager::widget(
            [
                'pagination' => $paginacao, 
                'linkContainerOptions' => [
                    'class' => 'page-item bg-dark text-info border-info'
                    ]
                , 'linkOptions' => [
                    'class' => 'page-link bg-dark text-info border-info'
                ],
                'disabledListItemSubTagOptions' => [
                    'class' => 'page-link bg-dark text-info border-info'
                ]
            ]
            ) ?>
    </div>
</div>

<?=modalComponent::initModal()?>