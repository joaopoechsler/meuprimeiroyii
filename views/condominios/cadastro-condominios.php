<?
use app\components\estadosComponent;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\controllers\AdministradorasController;
?>

<h1 class="text-center text-dark">Cadastro de Condominios</h1>

<form id="form-condo" class="col-12 py-2 mt-4 mb-4 rounded shadow" action="<?= Url::to(['condominios/realiza-cadastro-condominios']); ?>" method="post">
    <input class="col col-12 mt-2 form-control" type="text" name="nome" value="" placeholder="Nome" required>
    <input class="col col-12 mt-2 form-control" type="text" name="qtblocos" value="" placeholder="Quantidade de Blocos" required>

    <input class="col col-12 mt-4 form-control" type="text" name="rua" value="" placeholder="Rua" required>
    <input class="col col-12 mt-2 form-control" type="text" name="num" value="" placeholder="Número" required>
    <input class="col col-12 mt-2 form-control" type="text" name="bairro" value="" placeholder="Bairro" required>
    <input class="col col-12 mt-2 form-control" type="text" name="cidade" value="" placeholder="Cidade" required>

    <select name="from_administradora" class="col col-12 mt-2 form-control">
        <option value="">Selecione a Administradora</option>
        <?
        foreach (AdministradorasController::listaAdministradorasSelect() as $adm) {
        ?>
            <option value="<?= $adm['id'] ?>"><?= $adm['nomeAdm'] ?></option>
        <? } ?>
    </select>

    <select id="inputState" class="col col-12 mt-2 form-control" name="from_estado" required>
        <option value="">selecionar o Estado</option>
        <?
        foreach (estadosComponent::estados() as $sig => $uf) { ?>
            <option value="<?= $sig ?>"><?= $uf ?></option>
        <? } ?>
    </select>

    <input class="col col-12 mt-2 form-control" type="text" name="cep" value="" placeholder="Cep" required>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">

    <button class="btn btn-info mt-2 col col-12 buttonEnviar" type="submit">Enviar</button>
</form>