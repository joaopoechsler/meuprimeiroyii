<?

use app\components\estadosComponent;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\controllers\AdministradorasController;
use app\components\modalComponent;
use app\components\selectedComponent;
?>

<h1 class="text-center text-dark">Editar Condominio</h1>

<form id="form-condo" class="col-12 py-2 mt-4 mb-4 rounded" action="<?= Url::to(['condominios/realiza-edicao-condominios']); ?>" method="post">
    <input class="col col-12 mt-2 form-control" type="text" name="nome" value="<?= $edit['nome'] ?>" placeholder="Nome" required>
    <input class="col col-12 mt-2 form-control" type="text" name="qtblocos" value="<?= $edit['qtblocos'] ?>" placeholder="Quantidade de Blocos" required>

    <input class="col col-12 mt-4 form-control" type="text" name="rua" value="<?= $edit['rua'] ?>" placeholder="Rua" required>
    <input class="col col-12 mt-2 form-control" type="text" name="num" value="<?= $edit['num'] ?>" placeholder="Número" required>
    <input class="col col-12 mt-2 form-control" type="text" name="bairro" value="<?= $edit['bairro'] ?>" placeholder="Bairro" required>
    <input class="col col-12 mt-2 form-control" type="text" name="cidade" value="<?= $edit['cidade'] ?>" placeholder="Cidade" required>

    <select name="from_administradora" class="custom-select mt-2 fromAdministradora">
        <option value="">Selecione um Condomínio</option>
        <?
        foreach (AdministradorasController::listaAdministradorasSelect() as $adm) { ?>
            <option value="<?= $adm['id'] ?>" <?= selectedComponent::isSelected($adm['id'], $edit['from_administradora']) ?>><?= $adm['nomeAdm'] ?></option>
        <? } ?>
    </select>

    <select id="inputState" class="form-control mt-2" name="estado" required>
        <option value="">selecionar</option>
        <?
        foreach (estadosComponent::estados() as $sig => $uf) { ?>
            <option value="<?= $sig ?>" <?= selectedComponent::isSelected($sig, $edit['estado']) ?>><?= $uf ?></option>
        <? } ?>
    </select>

    <input type="hidden" name="id" value="<?= $edit['id'] ?>">

    <input class="col col-12 mt-2 form-control" type="text" name="cep" value="<?= $edit['cep'] ?>" placeholder="Cep" required>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">

    <div class="col col-12 mt-2 mx-0 form-group">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button class="btn btn-primary buttonEnviar" type="submit">Salvar</button>
    </div>
    <!-- <button class="btn btn-info mt-2 col col-12 buttonEnviar" type="submit">Enviar</button> -->
</form>