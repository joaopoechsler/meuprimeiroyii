<?
use app\components\maskComponent;
use app\components\modalComponent;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\url;


$url_site = Url::base($schema = true);

?>
<h1 class="text-center">Condominios</h1>
<ul>

<table class="container-fluid table table-dark table-striped table-responsive-md mt-4 shadow" id="listaCondo">
    <tr>
        <td>Nome</td>
        <td>Administradora</td>
        <td>Blocos</td>
        <td>Rua</td>
        <td>N°</td>
        <td>Bairro</td>
        <td>Cidade</td>
        <td>Estado</td>
        <td>Cep</td>
        <td>Data Cadastro</td>
        <td>Data Att</td>
        <td></td>
        <td align="center"><a href="<?=$url_site?>cadastroCondominio" class="btn btn-info">Cadastrar</a></td>
    </tr>

    <?
    foreach ($condominios as $dadosCondo) {
    ?>
        <tr data-id="<?=$dadosCondo['id']?>">
            <td><?= $dadosCondo['nome'] ?></td>
            <td><?= $dadosCondo['AdmNome'] ?></td>
            <td><?= $dadosCondo['qtblocos'] ?></td>
            <td><?= $dadosCondo['rua'] ?></td>
            <td><?= $dadosCondo['num'] ?></td>
            <td><?= $dadosCondo['bairro'] ?></td>
            <td><?= $dadosCondo['cidade'] ?></td>
            <td><?= $dadosCondo['estado'] ?></td>
            <td><?= maskComponent::mask($dadosCondo['cep'], 'cep') ?></td>
            <td><?=Yii::$app->formatter->format($dadosCondo['dataCadastro'],'date')?></td>
            <td align="center">
                <a href="<?=$url_site?>/index.php?r=condominios/edita-condominios&id=<?= $dadosCondo['id'] ?>" class="openModal"><i class="bi bi-pencil-square text-info"></i></a>
                <a href="#" data-id="<?= $dadosCondo['id'] ?>" class="removerCondo"><i class="bi bi-trash-fill text-info"></i></a>
            </td>
            <td></td>
            <td></td>
        </tr>
    <? } ?>
</table>

<div class="totalRegistros col-12 float-right">
    Total Registros <?=$paginacao->totalCount?>
</div>

<div class="row">
    <div class="col-12 mt-2">
            <?= LinkPager::widget(
            [
                'pagination' => $paginacao, 
                'linkContainerOptions' => [
                    'class' => 'page-item bg-dark text-info border-info'
                    ]
                , 'linkOptions' => [
                    'class' => 'page-link bg-dark text-info border-info'
                ],
                'disabledListItemSubTagOptions' => [
                    'class' => 'page-link bg-dark text-info border-info'
                ]
            ]
            ) ?>
    </div>
</div>

<?=modalComponent::initModal()?>