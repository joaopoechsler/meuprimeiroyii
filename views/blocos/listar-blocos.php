<?

use app\components\modalComponent;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\url;

$url_site = Url::base($schema = true);

?>
<h1 class="text-center">Blocos</h1>
<ul>

<table class="container-fluid table table-dark table-striped table-responsive-md mt-4" id="listaBloco">
    <tr>
        <td>Nome</td>
        <td>Condominio</td>
        <td>Andares</td>
        <td>Qtd unidades/Andar</td>
        <td>Data Cadastro</td>
        <td align="center"><a href="cadastroCondominio" class="btn btn-info">Cadastrar</a></td>
        <td colspan="2"></td>
    </tr>

    <?
    foreach ($blocos as $dadosBloco) {
    ?>
        <tr data-id="<?=$dadosBloco['id']?>">
            <td><?= $dadosBloco['nomeBloco'] ?></td>
            <td><?= $dadosBloco['nome'] ?></td>
            <td><?= $dadosBloco['Andares'] ?></td>
            <td><?= $dadosBloco['qtUnidadesAndar'] ?></td>
            <td><?=Yii::$app->formatter->format($dadosBloco['dataCadastro'],'date')?></td>
            <td align="center">
                <a href="<?=$url_site?>/index.php?r=blocos/edita-blocos&id=<?= $dadosBloco['id'] ?>" class="openModal"><i class="bi bi-pencil-square text-info"></i></a>
                <a href="<?=$url_site?>?r=blocos/deleta-blocos&id=<?=$dadosBloco['id']?>" data-id="<?= $dadosBloco['id'] ?>" class="removerCondo"><i class="bi bi-trash-fill text-info"></i></a>
            </td>
            <td></td>
            <td></td>
        </tr>
    <? } ?>
</table>

<div class="totalRegistros col-12 float-right">
    Total Registros <?=$paginacao->totalCount?>
</div>

<div class="row">
    <div class="col-12 mt-2">
            <?= LinkPager::widget(
            [
                'pagination' => $paginacao, 
                'linkContainerOptions' => [
                    'class' => 'page-item bg-dark text-info border-info'
                    ]
                , 'linkOptions' => [
                    'class' => 'page-link bg-dark text-info border-info'
                ],
                'disabledListItemSubTagOptions' => [
                    'class' => 'page-link bg-dark text-info border-info'
                ]
            ]
            ) ?>
    </div>
</div>

<?=modalComponent::initModal()?>