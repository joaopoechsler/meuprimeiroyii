<?

use app\components\selectedComponent;
use app\components\modalComponent;
use app\controllers\CondominiosController;
use yii\helpers\Url;
?>

<div class="row justify-content-center">

    <h1 class="text-center text-dark">Cadastro de Bloco</h1>
    <form id="form-bloco" class="col-12 px-2 py-2 mt-4 mb-4 rounded" action="<?=Url::to(['blocos/realiza-edicao-blocos']);?>" method="post">

    <select name="from_condominio" class="custom-select mt-2 fromAdministradora">
    <option value="">Selecione um Condomínio</option>
                <? 
               foreach(CondominiosController::listaCondominiosSelect() as $cond){?>
                <option value="<?=$cond['id']?>"<?=selectedComponent::isSelected($cond['id'], $edit['from_condominio'])?>><?=$cond['nome']?></option>
                <? } ?>
    </select>

        <input class="col col-12 mt-2 form-control" type="text" name="nomeBloco" value="<?= $edit['nomeBloco'] ?>" placeholder="Nome do Bloco" required>
        <input class="col col-12 mt-2 form-control" type="text" name="Andares" value="<?= $edit['Andares'] ?>" placeholder="Andares" required>
        <input class="col col-12 mt-2 form-control" type="text" name="qtUnidadesAndar" value="<?= $edit['qtUnidadesAndar'] ?>" placeholder="Qtd de Unidades por Andar">

        <input type="hidden" name="id" value="<?=$edit['id']?>">

        <input type="hidden" name="<?= yii::$app->request->csrfParam; ?>" value="<?= yii::$app->request->csrfToken; ?>">

        <div class="col col-12 mt-2 mx-0 form-group">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button class="btn btn-primary buttonEnviar" type="submit">Salvar</button>
        </div>
        <!-- <button class="btn btn-info mt-2 col col-12 buttonEnviar" type="submit">Enviar</button> -->
    </form>
</div>