<?
use app\controllers\CondominiosController;
use yii\helpers\Url;
?>

<div class="row justify-content-center">

    <h1 class="text-center text-dark">Cadastro de Bloco</h1>
    <form id="form-bloco" class="col-12 px-2 py-2 mt-4 mb-4 rounded shadow" action="<?=Url::to(['blocos/realiza-cadastro-blocos']);?>" method="post">

        <select name="from_condominio" class="custom-select mt-2">
            <option value="">Selecione o Condominio</option>
            <?
            foreach (CondominiosController::listaCondominiosSelect() as $condo) {
            ?>
                <option value="<?= $condo['id'] ?>"><?= $condo['nome'] ?></option>
            <? } ?>
        </select>

        <input class="col col-12 mt-2 form-control" type="text" name="nomeBloco" value="" placeholder="Nome do Bloco" required>
        <input class="col col-12 mt-2 form-control" type="text" name="Andares" value="" placeholder="Andares" required>
        <input class="col col-12 mt-2 form-control" type="text" name="qtUnidadesAndar" value="" placeholder="Qtd de Unidades por Andar">

        <input type="hidden" name="<?= yii::$app->request->csrfParam; ?>" value="<?= yii::$app->request->csrfToken; ?>">

        <button class="btn btn-info mt-2 col col-12 buttonEnviar" type="submit">Enviar</button>
    </form>
</div>