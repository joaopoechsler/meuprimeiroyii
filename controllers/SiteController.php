<?php

namespace app\controllers;

use Yii;
use app\models\LoginForm;
use yii\web\Controller;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionLogin()
    {
        $this->layout = false;
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $request = \yii::$app->request;

        if ($request->isPost) {
            $identity = LoginForm::findOne(['usuario' => $request->post('usuario'), 'senha' => $request->post('senha')]);
            if ($identity) {
                Yii::$app->user->login($identity);
                return $this->redirect(['index']);
            } else {
                return $this->redirect([
                    '',
                    'myAlert' =>
                    [
                        'type' => 'warning',
                        'msg' => 'Dados de usuário não conferem',
                        'redir' => 'login'
                    ]
                ]);
            }
        }
        return $this->render('login');
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(['site/login']);
    }

    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        return $this->render('home');
    }

    public function actionCumprimentar($mensagem = 'Olá') 
    {
        return $this->render('cumprimentar', ['mensagem' => $mensagem]);
    }
}
