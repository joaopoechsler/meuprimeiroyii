<?
namespace app\controllers;

use app\models\AdministradorasModel;
use yii\web\Controller;         
use yii\data\Pagination;

class AdministradorasController extends Controller {

    public static function listaAdministradorasSelect() {
        $query = AdministradorasModel::find();

        return $query->orderBy('id')->all();
    }

    public function actionCadastraAdministradoras() {
        return $this->render('cadastro-administradoras');
    }

    public function actionRealizaCadastroAdministradora()
    {
        $request = \yii::$app->request;

        if ($request->isPost) {
            $model = new AdministradorasModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['administradoras/listar-administradoras']);
        }

        return $this->render('cadastro-administradoras');
    }

    public function actionEditaAdministradoras() 
    {
        $request = \yii::$app->request;
        $this->layout = false;
        if ($request->isGet) {
            $query = AdministradorasModel::find();
            $administradoras = $query->where(['id' => $request->get()])->one();
        }

        return $this->render('edita-administradoras', [
            'edit' => $administradoras
        ]);
    }

    public function actionRealizaEdicaoAdministradoras() {
        $request = \yii::$app->request;

        if($request->isPost){

            $model = AdministradorasModel::findOne($request->post('id'));
            $model->attributes = $request->post();

            if($model->update()){
                return $this->redirect(['administradoras/listar-administradoras']);
            } else {
                return $this->redirect(['administradoras/listar-administradoras']);
            }
        }  
    }

    public function actionListarAdministradoras() {

        $query = (new \yii\db\Query())
        ->select(
        'adm.id,
        adm.nomeAdm,
        adm.cnpj,
        adm.dataCadastro'
        )
        ->from('jp_administradora adm');

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $administradoras = $query->orderBy('nomeAdm')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();
        
        return $this->render('listar-administradoras',[
            'administradoras' => $administradoras,
            'paginacao' => $paginacao,
        ]);

    }
    
}

?>