<?
namespace app\controllers;

use app\models\UnidadesModel;
use yii\web\Controller;         
use yii\data\Pagination;


class UnidadesController extends Controller {

    public static function listaUnidadesSelect() {
        $query = UnidadesModel::find();

        return $query->orderBy('id')->all();
    }

    public function actionListaUnidadesApi()
    {
        $request = \yii::$app->request;
        $query = UnidadesModel::find();
        $data = $query->where(['from_bloco' => $request->post()])->orderBy('numUnd')->all();

        $dadosUnd = array();
        $i = 0;

        foreach($data as $d) {
            $dadosUnd[$i]['id'] = $d['id'];
            $dadosUnd[$i]['numUnd'] = $d['numUnd'];
            $i++;
        }
        return json_encode($dadosUnd);
    }

    public function actionCadastroUnidades() {
        return $this->render('cadastro-unidades');
    }

    public function actionRealizaCadastroUnidades()
    {
        $request = \yii::$app->request;

        if ($request->isPost) {
            $model = new UnidadesModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['unidades/listar-unidades']);
        }

        return $this->render('cadastro-unidades');
    }

    public function actionListarUnidades() {

        $query = (new \yii\db\Query())
        ->select('unidade.id, 
        unidade.numUnd, 
        bloco.nomeBloco, 
        condo.nome, 
        unidade.from_condominio, 
        unidade.from_bloco, 
        unidade.metragem, 
        unidade.qtVagas,
        unidade.dataCadastro'
        )
        ->from('jp_unidade unidade')
        ->innerJoin('jp_bloco bloco', 'bloco.id = unidade.from_bloco')
        ->innerJoin('jp_condominio condo', 'condo.id = unidade.from_condominio');

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $unidades = $query->orderBy('numUnd')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();
        
        return $this->render('listar-unidades',[
            'unidades' => $unidades,
            'paginacao' => $paginacao,
        ]);

    }
    
}

?>