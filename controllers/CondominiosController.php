<?
namespace app\controllers;

use Yii;
use app\models\CondominiosModel;
use yii\web\Controller;         
use yii\data\Pagination;


class CondominiosController extends Controller {

    public static function listaCondominiosSelect() {
        $query = CondominiosModel::find();

        return $query->orderBy('id')->all();
    }

    public function actionCadastroCondominios() {
        return $this->render('cadastro-condominios');
    }

    public function actionRealizaCadastroCondominios()
    {
        $request = \yii::$app->request;

        if ($request->isPost) {
            $model = new CondominiosModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['condominios/listar-condominios']);
        }

        return $this->render('cadastro-condominios');
    }

    public function actionEditaCondominios() 
    {
        $request = \yii::$app->request;
        $this->layout = false;
        if ($request->isGet) {
            $query = CondominiosModel::find();
            $condominios = $query->where(['id' => $request->get()])->one();
        }

        return $this->render('edita-condominios', [
            'edit' => $condominios
        ]);
    }

    public function actionRealizaEdicaoCondominios() {
        $request = \yii::$app->request;

        if($request->isPost){

            $model = CondominiosModel::findOne($request->post('id'));
            $model->attributes = $request->post();

            if($model->update()){
                return $this->redirect(['condominios/listar-condominios']);
            } else {
                return $this->redirect(['condominios/listar-condominios']);
            }
        }  
    }
    
    public function actionListarCondominios() {

        if(Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $query = (new \yii\db\Query())
        ->select(
        'cond.id,
        cond.from_administradora,
        adm.nomeAdm as AdmNome,
        cond.nome,
        cond.qtblocos,
        cond.cep,
        cond.rua,
        cond.num,
        cond.bairro,
        cond.cidade,
        cond.estado,
        cond.dataCadastro'
        )
        ->from('jp_condominio cond')
        ->innerJoin('jp_administradora adm', 'adm.id = cond.from_administradora');

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $condominios = $query->orderBy('nome')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();
        
        return $this->render('listar-condominios',[
            'condominios' => $condominios,
            'paginacao' => $paginacao,
        ]);

    }
    
}

?>