<?
namespace app\controllers;

use app\models\ConselhoModel;
use yii\web\Controller;         
use yii\data\Pagination;


class ConselhoController extends Controller {

    public static function listaConselhoSelect() 
    {
        $query = ConselhoModel::find();

        return $query->orderBy('id')->all();
    }

    public function actionListaConselhoApi()
    {
        $request = \yii::$app->request;
        $query = ConselhoModel::find();
        $data = $query->where(['from_condominio' => $request->post()])->orderBy('nomeFunc')->all();

        $dados = array();
        $i = 0;

        foreach($data as $d) {
            $dados[$i]['id'] = $d['id'];
            $dados[$i]['nomeFunc'] = $d['nomeFunc'];
            $i++;
        }
        return json_encode($dados);
    }

    public function actionCadastroConselho() {
        return $this->render('cadastro-conselho');
    }

    public function actionRealizaCadastroConselho()
    {
        $request = \yii::$app->request;

        if ($request->isPost) {
            $model = new ConselhoModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['Conselho/cadastro-conselho']);
        }

        return $this->render('cadastro-conselho');
    }

    public function actionListarConselho() {

        $query = (new \yii\db\Query())
        ->select('
        conselho.id,
        condo.nome,
        conselho.NomeFunc,
        conselho.funcao'
        )
        ->from('conselho conselho')
        ->innerJoin('jp_condominio condo', 'condo.id = conselho.from_condominio');

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $Conselho = $query->orderBy('nomeBloco')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();
        
        return $this->render('listar-Conselho',[
            'Conselho' => $Conselho,
            'paginacao' => $paginacao,
        ]);

    }
    
}

?>