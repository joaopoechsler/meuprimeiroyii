<?
namespace app\controllers;

use app\components\alertComponent;
use app\models\MoradoresModel;
use yii\web\Controller;         
use yii\data\Pagination;


class MoradoresController extends Controller {

    public function actionCadastroMoradores() {
        return $this->render('cadastro-moradores');
    }

    public function actionRealizaCadastroMoradores()
    {
        $request = \yii::$app->request;

        if ($request->isPost) {
            $model = new MoradoresModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['moradores/cadastro-moradores']);
        }

        return $this->render('cadastro-moradores');
    }

    public function actionEditaMoradores() 
    {
        $request = \yii::$app->request;
        $this->layout = false;
        if ($request->isGet) {
            $query = MoradoresModel::find();
            $moradores = $query->where(['id' => $request->get()])->one();
        }

        return $this->render('edita-moradores', [
            'edit' => $moradores
        ]);
    }

    public function actionRealizaEdicaoMoradores() {
        $request = \yii::$app->request;

        if($request->isPost){

            $model = MoradoresModel::findOne($request->post('id'));
            $model->attributes = $request->post();

            if($model->update()){
                return $this->redirect(['moradores/listar-moradores']);
            } else {
                return $this->redirect(['moradores/listar-moradores']);
            }
        }  
    }

    public function actionDeletaMorador()
    {
        $request = \yii::$app->request;

        if ($request->isGet) {
            $model = MoradoresModel::findOne($request->get('id'));
            if ($model->delete()) {
                return $this->redirect(['moradores/listar-moradores',
                    'myAlert' => [
                        'type' => 'sucess',
                        'msg' => 'Registro Deletado'
                    ] 
                ]);
            } else {
                return $this->redirect(['moradores/listar-moradores',
                'myAlert' => [
                    'type' => 'danger',
                    'msg' => 'Registro não Deletado'
                ]
                ]);
            }
        }
    }

    public function actionListarMoradores() 
    {

        $query = (new \yii\db\Query())
        ->select('morador.id,  
        bloco.nomeBloco, 
        condo.nome,
        und.numUnd,
        morador.from_condominio,
        morador.from_bloco,
        morador.from_unidade,
        morador.nomeMorador,
        morador.dataCadastro,
        morador.cpf,
        morador.email,
        morador.telefone,
        morador.genero,
        morador.nascimento'
        )
        ->from('jp_morador morador')
        ->innerJoin('jp_bloco bloco', 'bloco.id = morador.from_bloco')
        ->innerJoin('jp_condominio condo', 'condo.id = morador.from_condominio')
        ->innerJoin('jp_unidade und', 'und.id = morador.from_unidade');

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $moradores = $query->orderBy('nomeMorador')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();
        
        return $this->render('listar-moradores',[
            'moradores' => $moradores,
            'paginacao' => $paginacao,
        ]);

    }
    
}

?>