<?
namespace app\components;

use yii\base\Component;

class modalComponent extends Component{

    public static function initModal(){
        $estrutura = "<div class=\"modal fade\" id=\"modalComponent\" tabindex=\"-1\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog modal-lg\">
            <div class=\"modal-content modal-xl\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" id=\"exampleModalLabel\">Edição</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            <div class=\"modal-body\">
                {{injector}}
            </div>
            </div>
        </div>
        </div>";

        return $estrutura;
    }
}
?>