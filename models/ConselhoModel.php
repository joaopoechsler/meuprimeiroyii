<?
namespace app\models;

use yii\db\ActiveRecord;

Class ConselhoModel extends ActiveRecord {

    public static function tableName()
    {
        return 'jp_conselho';
    }

    public function rules()
    {
        return [
            [['NomeFunc', 'funcao', 'from_condominio'], 'required']
        ];
    }
}

?>