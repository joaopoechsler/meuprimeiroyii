<?
namespace app\models;

use yii\db\ActiveRecord;

Class UnidadesModel extends ActiveRecord {

    public static function tableName()
    {
        return 'jp_unidade';
    }

    public function rules()
    {
        return [
            [['from_condominio', 'from_bloco', 'numUnd', 'metragem', 'qtVagas'], 'required'],
        ];
    }

}

?>