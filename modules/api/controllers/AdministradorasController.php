<?

namespace app\modules\api\controllers;

use app\models\AdministradorasModel;
use Exception;
use yii\web\Controller;

class AdministradorasController extends Controller
{

    public function actionGetAll()
    {
        $qry = AdministradorasModel::find();
        $data = $qry->orderBy('nomeAdm')->all();
        $dados = [];
        $i = 0;

        if ($qry->count() > 0) {
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            foreach ($data as $d) {
                foreach ($d as $ch => $r) {
                    $dados['resultSet'][$i][$ch] = $r;
                }
                $i++;
            }
        } else {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }

        return json_encode($dados);
    }

    public function actionGetOne()
    {
        $request = \yii::$app->request;
        $qry = AdministradorasModel::find();
        $d = $qry->where(['id' => $request->get('id')])->one();

        if ($qry->count() > 0) {
            $dados['endPoint']['status'] = 'success';
            foreach ($d as $ch => $r) {
                $dados['resultSet'][0][$ch] = $r;
            }
        } else {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }

        return json_encode($dados);
    }

    //adicionar um novo registro
    public function actionRegisterAdm()
    {
        $request = \yii::$app->request;

        try {
            if ($request->isPost) {
                $dados = [];
                $model = new AdministradorasModel();
                $model->attributes = $request->post();
                $model->save();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro inserido com sucesso.';

                return json_encode($dados);
            }
        } catch (Exception $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';

            return json_encode($dados);
        }
    }

    //editar um registro
    public function actionEditAdm()
    {
        $request = \yii::$app->request;

        try {
            if ($request->isPost) {
                $model = AdministradorasModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro editado com sucesso.';

                return json_encode($dados);
            }
        } catch (Exception $e) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Registro não inserido';

            return json_encode($dados);
        }
    }

    //excluir registro
    public function actionDelete()
    {
        $r = \yii::$app->request;

        try {
            if ($r->isPost) {
                $model = AdministradorasModel::findOne($r->post('id'));
                $model->delete();
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro editado com sucesso.';

                return json_encode($dados);
            }
        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['error'] = $th;
            $dados['endPoint']['msg'] = 'Registro não deletado';

            return json_encode($dados);
        }
    }
}
