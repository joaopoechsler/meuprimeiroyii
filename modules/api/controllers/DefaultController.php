<?php

namespace app\modules\api\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{

    public function actionIndex()
    {
        $this->layout = false;
        return $this->render('index');
    }

    public function actionGetTokenPost()
    {
        $fieldName = \yii::$app->request->csrfParam;
        $tokenValue = \yii::$app->request->csrfToken;

        if ($fieldName && $tokenValue) {
            $dados = [
                'field' => $fieldName,
                'token' => $tokenValue
            ];
            return json_encode($dados);
        } else {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }
    }
}
