<?

namespace app\modules\api\controllers;

use app\models\AdministradorasModel;
use app\models\CondominiosModel;
use yii\web\Controller;

class CondominiosController extends Controller
{

    public function actionGetAll()
    {
        $qry = (new \yii\db\Query())
        ->select('cond.id,
        cond.from_administradora,
        adm.nomeAdm,
        cond.nome,
        cond.qtblocos,
        cond.cep,
        cond.rua,
        cond.num,
        cond.bairro,
        cond.cidade,
        cond.estado,
        cond.dataCadastro')
        ->from(CondominiosModel::tableName().' cond')
        ->innerJoin('jp_administradora adm','adm.id = cond.from_administradora');
        $data = $qry->orderBy('nome')->all();
        $dados = [];
        $i = 0;

        if ($qry->count() > 0) {
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            foreach ($data as $d) {
                foreach ($d as $ch => $r) {
                    $dados['resultSet'][$i][$ch] = $r;
                }
                $i++;
            }
        } else {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Nao existem dados';
        }
        return json_encode($dados);
    }

    public function actionGetOne()
    {
        $request = \yii::$app->request;
        $qry = (new \yii\db\Query())
            ->select('cond.id,
            cond.idAdm as idADM,
            adm.nomeAdm,
            cond.nomeCondominio,
            cond.qtBlocos,
            cond.cep,
            cond.logradouro,
            cond.numero,
            cond.bairro,
            cond.cidade,
            cond.estado,
            cond.dataCadastro')
            ->from(CondominiosModel::tableName() . ' cond')
            ->innerJoin(AdministradorasModel::tableName() . ' adm', 'adm.id = cond.idAdm');
        $d = $qry->where(['cond.id' => $request->get('id')])->one();

        if ($qry->count() > 0) {
            $dados['endPoint']['status'] = 'success';
            foreach ($d as $ch => $r) {
                $dados['resultSet'][0][$ch] = $r;
            }
        } else {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Nao existe registro';
        }
        return json_encode($dados);
    }

    public function actionGetCondominioFromAdm(){
        $request = \yii::$app->request;
        $qry = CondominiosModel::find();
        $data = $qry->where(['from_administradora' => $request->get('from_administradora')])->orderBy('nome')->all();
        $dados = [];

        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            $i = 0;
            foreach($data as $d){
                $dados['resultSet'][$i]['id'] = $d['id'];
                $dados['resultSet'][$i]['nome'] = $d['nome'];
                $i++;
            }
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }

        return json_encode($dados);
    }

    public function actionRegisterCondo()
    {
        $request = \yii::$app->request;

        try {
            if ($request->isPost) {
                $dados = [];
                $model = new CondominiosModel();
                $model->attributes = $request->post();
                $model->save();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro inserido com sucesso.';

                return json_encode($dados);
            }
        } catch (\Exception $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';

            return json_encode($dados);
        }
    }

    //editar um registro
    public function actionEditCondo()
    {
        $request = \yii::$app->request;

        try {
            if ($request->isPost) {
                $model = CondominiosModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro editado com sucesso.';

                return json_encode($dados);
            }
        } catch (\Exception $e) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Registro não inserido';

            return json_encode($dados);
        }
    }

    //excluir registro
    public function actionDeleteCondo()
    {
        $r = \yii::$app->request;

        try {
            if ($r->isPost) {
                $model = CondominiosModel::findOne($r->post('id'));
                $model->delete();
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro editado com sucesso.';

                return json_encode($dados);
            }
        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['error'] = $th;
            $dados['endPoint']['msg'] = 'Registro não deletado';

            return json_encode($dados);
        }
    }


}
