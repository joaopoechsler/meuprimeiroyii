<?

namespace app\modules\api\controllers;

use app\models\MoradoresModel;
use Exception;
use yii\web\Controller;

class MoradoresController extends Controller
{

    public function actionGetAll()
    {
        $qry = (new \yii\db\Query())
            ->select(
                'unidade.id, 
                unidade.numUnd, 
                bloco.nomeBloco, 
                condo.nome, 
                unidade.from_condominio, 
                unidade.from_bloco, 
                unidade.metragem, 
                unidade.qtVagas,
                unidade.dataCadastro'
            )
            ->from('jp_unidade unidade')
            ->innerJoin('jp_bloco bloco', 'bloco.id = unidade.from_bloco')
            ->innerJoin('jp_condominio condo', 'condo.id = unidade.from_condominio');

        $data = $qry->orderBy('numUnd')->all();
        $dados = [];
        $i = 0;

        if ($qry->count() > 0) {
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            foreach ($data as $d) {
                foreach ($d as $ch => $r) {
                    $dados['resultSet'][$i][$ch] = $r;
                }
                $i++;
            }
        } else {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }

        return json_encode($dados);
    }

    public function actionGetOne()
    {
        $request = \yii::$app->request;
        $qry = (new \yii\db\Query())
            ->select(
                'unidade.id, 
                unidade.numUnd, 
                bloco.nomeBloco, 
                condo.nome, 
                unidade.from_condominio, 
                unidade.from_bloco, 
                unidade.metragem, 
                unidade.qtVagas,
                unidade.dataCadastro'
            )
            ->from('jp_unidade unidade')
            ->innerJoin('jp_bloco bloco', 'bloco.id = unidade.from_bloco')
            ->innerJoin('jp_condominio condo', 'condo.id = unidade.from_condominio');

        $d = $qry->where(['id' => $request->get('id')])->one();

        if ($qry->count() > 0) {
            $dados['endPoint']['status'] = 'success';
            foreach ($d as $ch => $r) {
                $dados['resultSet'][0][$ch] = $r;
            }
        } else {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }

        return json_encode($dados);
    }

    //adicionar um novo registro
    public function actionRegisterUnidade()
    {
        $request = \yii::$app->request;

        try {
            if ($request->isPost) {
                $dados = [];
                $model = new MoradoresModel();
                $model->attributes = $request->post();
                $model->save();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro inserido com sucesso.';

                return json_encode($dados);
            }
        } catch (Exception $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';

            return json_encode($dados);
        }
    }

    //editar um registro
    public function actionEditUnidade()
    {
        $request = \yii::$app->request;

        try {
            if ($request->isPost) {
                $model = MoradoresModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro editado com sucesso.';

                return json_encode($dados);
            }
        } catch (Exception $e) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Registro não inserido';

            return json_encode($dados);
        }
    }

    //excluir registro
    public function actionDeleteUnidade()
    {
        $r = \yii::$app->request;

        try {
            if ($r->isPost) {
                $model = MoradoresModel::findOne($r->post('id'));
                $model->delete();
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro editado com sucesso.';

                return json_encode($dados);
            }
        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['error'] = $th;
            $dados['endPoint']['msg'] = 'Registro não deletado';

            return json_encode($dados);
        }
    }
}
