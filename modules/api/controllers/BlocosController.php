<?

namespace app\modules\api\controllers;

use app\models\BlocosModel;
use yii\web\Controller;

class BlocosController extends Controller
{

    public function actionGetAll()
    {
        $qry = (new \yii\db\Query())
            ->select(
                'condo.id as idCondo,
                bloco.id,
                condo.nome,
                bloco.nomeBloco,
                bloco.Andares,
                bloco.qtUnidadesAndar,
                bloco.dataCadastro'
            )
            ->from('jp_bloco bloco')
            ->innerJoin('jp_condominio condo', 'condo.id = bloco.from_condominio');

        $data = $qry->orderBy('nomeBloco')->all();
        $dados = [];
        $i = 0;

        if ($qry->count() > 0) {
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            foreach ($data as $d) {
                foreach ($d as $ch => $r) {
                    $dados['resultSet'][$i][$ch] = $r;
                }
                $i++;
            }
        } else {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Nao existem dados';
        }
        return json_encode($dados);
    }

    public function actionGetOne()
    {
        $request = \yii::$app->request;
        $qry = (new \yii\db\Query())
            ->select(
                'condo.id as idCondo,
                bloco.id,
                condo.nome,
                bloco.nomeBloco,
                bloco.Andares,
                bloco.qtUnidadesAndar,
                bloco.dataCadastro'
            )
            ->from('jp_bloco bloco')
            ->innerJoin('jp_condominio condo', 'condo.id = bloco.from_condominio');
        $d = $qry->where(['bloco.id' => $request->get('id')])->one();

        if ($qry->count() > 0) {
            $dados['endPoint']['status'] = 'success';
            foreach ($d as $ch => $r) {
                $dados['resultSet'][0][$ch] = $r;
            }
        } else {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Nao existe registro';
        }
        return json_encode($dados);
    }

    public function actionRegisterBloco()
    {
        $request = \yii::$app->request;

        try {
            if ($request->isPost) {
                $dados = [];
                $model = new BlocosModel();
                $model->attributes = $request->post();
                $model->save();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro inserido com sucesso.';

                return json_encode($dados);
            }
        } catch (\Exception $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';

            return json_encode($dados);
        }
    }

    //editar um registro
    public function actionEditBloco()
    {
        $request = \yii::$app->request;

        try {
            if ($request->isPost) {
                $model = BlocosModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro editado com sucesso.';

                return json_encode($dados);
            }
        } catch (\Exception $e) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Registro não inserido';

            return json_encode($dados);
        }
    }

    //excluir registro
    public function actionDeleteBloco()
    {
        $r = \yii::$app->request;

        try {
            if ($r->isPost) {
                $model = BlocosModel::findOne($r->post('id'));
                $model->delete();
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro editado com sucesso.';

                return json_encode($dados);
            }
        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['error'] = $th;
            $dados['endPoint']['msg'] = 'Registro não deletado';

            return json_encode($dados);
        }
    }
}
